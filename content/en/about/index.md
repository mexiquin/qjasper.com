+++
title = "My name is Quinton Jasper"
description = "About me"
type = "about"
date = "2022-07-27"
+++

I’m a professional software developer and incoming graduate student of Informatics at Northern Arizona University. What once started as an interest in programming and computing machinery has now become a desire to apply computing and statistical methods in order to identify real-world patterns and invoke real-world change. 

What do I specialize in? I'm not really sure just yet, as the applications are too numerous. However, I currently conduct research under Dr. Morgan Vigil-Hayes at the CANIS Lab here at NAU. Here, I utilize qualitative and quantitative statistical methods to better understand the effects of network and connectivity disparities among different socio-economic classes, as well as helping develop both software and community-engagement programs that both encourage and educate disadvantaged communities, allowing individuals to make full use of today's available technology.


